#ifndef IMAGE_H
#define IMAGE_H
#endif // IMAGE_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;



class image {
public:
    image();
    image(string fich);
    virtual ~image();

    string GetName();
    string GetFileNameHisto();
    string GetFileNameHistoNorm();



    //static int MyMethod( int * a, int * b );
    //MyClass::MyMethod( &two, &one );

    int getGreyLevel();
    vector<double> *GetHisto(string filename);
    vector<double> *GetNormHisto(string filename);
    void ComputeImage();
    static vector<double> *GetHistoFromFile(string filename);
    string GetFileNameAsciiPgm();
private:

    int dimX;
    int dimY;
    int greyLevel;
    void importITX();
    void exportImageAsciiPgm();
    void exportHisto();
    void exportHistoNorm();


    void LBP();
    void Histogram(vector<vector<int>> tableau);
    void initfile(string filename);
    void NormHisto();

    string orginalName;
    string asciipgmfilename;
    string csvfilename;
    string csvfoldername;
    string itxfilename;
    string csvNormfilename;

    vector< vector<int> > MatImage;
    vector<int> histo;
    vector<double> histoNorm;


};

