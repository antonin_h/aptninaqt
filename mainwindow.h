#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QtCharts>
#include "image.h"
#include <QDropEvent>
#include <QUrl>
#include <QDebug>

using namespace QtCharts;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_tabWidget_tabBarClicked(int index);
    void on_pushButton_clicked();


    void on_listWidgetFile_currentRowChanged(int currentRow);


    void on_radioBNormalised_clicked();

private:
    int newImage=0;
    image im;
    Ui::MainWindow *ui;
    QGraphicsScene *scene0;
    QPixmap image0;
    QChart *chart;
    QChartView *chartView;
    void qtchartInit(vector<double> *histograme);
    vector<double>*hist;
    void ComputeImage(string filename);
    void imageshow();
    vector<string> adresseStringHisto;
    vector<string> adresseStringHistoNorm;
    vector<string> adresseImage;

    void showHisto(int currentrow);
protected:
    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
};

#endif // MAINWINDOW_H
