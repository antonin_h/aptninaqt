#include "mainwindow.h"
#include "ui_mainwindow.h"
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    ui->setupUi(this);
    this->setAcceptDrops(true);
    ui->listWidgetFile->setAcceptDrops(true);

}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent* event)
{
    for (const QUrl &url : event->mimeData()->urls()) {
        if(!url.isEmpty())
        {
            ComputeImage(url.toLocalFile().toStdString());
        }
        else
            cout<<"testQUrl"<<endl;
    }
}

void MainWindow::ComputeImage(string filename)
{
    //int numberElement=ui->listWidgetFile->count();
    image im = image(filename);
    adresseStringHisto.push_back(im.GetFileNameHisto());
    adresseStringHistoNorm.push_back(im.GetFileNameHistoNorm());
    adresseImage.push_back(im.GetFileNameAsciiPgm());
    ui->listWidgetFile->addItem(QString::fromStdString(im.GetName()));
    im.ComputeImage();
}

void MainWindow::on_pushButton_clicked()
{
    //QString namefile = QFileDialog::getOpenFileName(this,"Image à Traiter") ;//ouvre la fênetre pour choisir le filtre
    QStringList files = QFileDialog::getOpenFileNames(
                this,
                "Select one or more files to open",
                "/home",
                "Images (*.itx)");



    for (QStringList::iterator it = files.begin();
         it != files.end();++it) {
        if(!(*it).isEmpty())
        {
            ComputeImage((*it).toStdString());
        }
        else
            cout<<"QStringList"<<endl;
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::qtchartInit(vector<double> *hist)
{
    if(hist == NULL)
        return;
    //![1]
    chart  = new QChart;
    chartView = new QChartView(chart);
    QBarSet *set0 = new QBarSet("Normalised Histo");

    for(int i=0;i<(hist->size()-1);i++){
        *set0 << hist->at(i);
    }
    //![1]

    //![2]
    QBarSeries *series = new QBarSeries();
    series->append(set0);
    //![2]

    //![3]
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Histogram");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    //![3]

    //![4]
    QStringList categories;
    for(int i=0;i<255;i++){
        //categories << QString::number(i);
    }

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);
    //![4]

    //![5]
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    //![5]

    //![6]
    //!   orinaly  QChartView *chartView = new QChartView(chart);
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    //![6]
    chartView->setAcceptDrops(true);
    ui->grido->addWidget(chartView,0,0);
}

void MainWindow::on_tabWidget_tabBarClicked(int index)
{
    if(index==1)
        imageshow();

}

void MainWindow::imageshow()
{
    int current_row = ui->listWidgetFile->currentRow();

    if(current_row!=-1)
    {
        QString file = QString::fromStdString(adresseImage.at(current_row));
        image0.load(file);
        scene0 = new QGraphicsScene(this);
        scene0->addPixmap(image0);
        scene0->setSceneRect(image0.rect());
        ui->graphicsView0->setScene(scene0);
    }
}


/*
void MainWindow::on_listWidgetFile_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(!(ui->listWidgetFile->currentItem()->text().isEmpty()))
    {
        current->
        qtchartInit(hist);
    }
}

void MainWindow::on_listWidgetFile_currentRowChanged(int currentRow)
{

}
*/

void MainWindow::showHisto(int currentRow)
{
    if(ui->listWidgetFile->count()!=-1)
    {
        string histfilename;
        if(ui->radioBNormalised->isChecked())
            histfilename = adresseStringHistoNorm.at(currentRow);
        else
            histfilename = adresseStringHisto.at(currentRow);

        hist = image::GetHistoFromFile(histfilename);
        qtchartInit(hist);
    }

}

void MainWindow::on_listWidgetFile_currentRowChanged(int currentRow)
{
    showHisto(currentRow);
}


void MainWindow::on_radioBNormalised_clicked()
{
    if(ui->listWidgetFile->count()!=-1)
    {
        showHisto(ui->listWidgetFile->currentRow());
    }
}
