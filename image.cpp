/*
 * image.cpp
 * Author: Antonin Hereson
 */

//
#include "mainwindow.h"
#include <string>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
using namespace std;
namespace fs = boost::filesystem;

image::image() {
    // TODO Auto-generated destructor stub
}

image::~image() {
    // TODO Auto-generated destructor stub
}

image::image(string filename){

    initfile(filename);
    //dimX = 0;
    //dimY = 0;
}

void image::initfile(string name)
{
    fs::path abspath(name);
    //abspath.has_root_path();
    fs::path absdir = abspath;
    absdir.remove_filename();
    fs::path originalName =abspath.filename();

    this->orginalName=originalName.replace_extension("").string();

    fs::path filenameCSV;

    filenameCSV = absdir;
    filenameCSV /= "HistrogramCsv";//We create the Histrogram folder
    if (fs::create_directory(filenameCSV))
        cerr << "error histo" << endl;
    filenameCSV /= originalName.replace_extension(".csv");
    this->csvfilename = filenameCSV.string();

    filenameCSV = absdir;
    filenameCSV /= "HistrogramNormalised";
    if (fs::create_directory(filenameCSV))
        cerr << "error histo norm" << endl;
    filenameCSV /= originalName.replace_extension(".csv");
    this->csvNormfilename=  filenameCSV.string();

    fs::path filenamePgmAscii = absdir;
    filenamePgmAscii /= "Image Ascii PGM";
    if (fs::create_directory(filenamePgmAscii))
        cerr << "error filenamePgmAscii" << endl;
    filenamePgmAscii /= originalName.replace_extension(".ascii.pgm");
    this->asciipgmfilename=filenamePgmAscii.string();

    this->itxfilename = abspath.string();



}

void image::ComputeImage(void)
{
    greyLevel = 255;
    histo=vector<int>(256,0);
    importITX();
    Histogram(MatImage);
    exportHisto();
    exportHistoNorm();
    exportImageAsciiPgm();
}


void image::importITX()
{
    ifstream file(itxfilename.c_str(), ios::in);
    if(file)  // si l'ouverture a réussi
    {
        string contenu;
        getline(file, contenu);
        getline(file, contenu);
        getline(file, contenu);
        getline(file, contenu);
        file >> dimX;
        file >> dimY;
        getline(file, contenu);
        int i=dimY-1; //Pour la largeur
        vector<int> ligne;
        int entier;
        while (!file.eof()){
            file >> entier;
            ligne.push_back(entier);
            i--;
            if (i<0){
                i=dimY-1;
                MatImage.push_back(ligne);
                ligne.clear();
            }
        }
        file.close();  // I close the file
    }
    else  // sinon

        cerr << "Error open !" << endl;
}



void image::exportImageAsciiPgm()
{

    ofstream file(asciipgmfilename.c_str(), ios::out | ios::trunc);
    if(file)  // if file open
    {
        file << "P2"<<endl;
        file << "# Image by Antonin Hereson for Nibec"<<endl;
        file << dimX <<" "<< dimY<<endl;
        file  << (int)255 <<endl;

        for(int i = 0;i<dimX;i++){
            for(int j = 0;j<dimY;j++){
                file << MatImage[i][j] << " ";
            }
            file << endl;
        }
        file.close();
    }
    else
        cerr << "error exportImagePgm" << endl;
}

void image::exportHistoNorm()
{
    ofstream file(csvNormfilename.c_str(), ios::out | ios::trunc);
    if(file)  // if file open
    {
        for(int i = 0;i<greyLevel;i++)
        {
            file << histoNorm[i] <<endl;
        }
        file.close();
    }
    else
        cerr << "error exportHisto" << endl;
}

void image::exportHisto()
{
    ofstream file(csvfilename.c_str(), ios::out | ios::trunc);
    if(file)  // if file open
    {
        for(int i = 0;i<greyLevel;i++)
        {
            file << histo[i] <<endl;
        }
        file.close();
    }
    else
        cerr << "error exportHisto" << endl;
}

void image::Histogram(vector< vector<int> > tableau){
    for(int i = 0;i<dimX;i++){
        for(int j = 0;j<dimY;j++){
            histo.at(tableau[i][j])++;
        }
    }
    NormHisto();
}


void image::NormHisto(void)
{
    auto biggest = max_element(histo.begin(), histo.end());
    double norm = (double)(*biggest);
    for(int i=0;i<255;i++){
        histoNorm.push_back( (double)histo.at(i) / norm);
    }
}

vector<double> *image::GetHistoFromFile(string filename)
{
    vector<double> *histReturn = new vector<double>();
    ifstream file(filename.c_str(), ios::in);
    double number;
    if(file)  // si l'ouverture a réussi
    {
        while (!file.eof()){
            file >> number;
            histReturn->push_back(number);
        }
        file.close();  // I close the file
    }
    else  // sinon
        return NULL;

    return histReturn;

}


int image::getGreyLevel(){
    return greyLevel;
}

string image::GetName()
{
    return orginalName;
}

string image::GetFileNameHisto()
{
    return csvfilename;
}

string image::GetFileNameHistoNorm()
{
    return csvNormfilename;
}

string image::GetFileNameAsciiPgm()
{
    return asciipgmfilename;
}

